package com.weather.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.weather.data.model.entity.City;

/**
 * @author Ehtiram_Abdullayev on 10/8/2018
 * @project weatherapp
 */
public interface CityRepository extends JpaRepository<City, Long> {

	List<City> findAll();

}