package com.weather.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.weather.data.model.entity.CityWeather;

/**
 * @author Ehtiram_Abdullayev on 9/24/2018
 * @project weatherapp
 */
@Repository
public interface WeatherRepository extends JpaRepository<CityWeather, Long> {

	@Query("SELECT cw1 FROM CityWeather cw1, City c1 WHERE cw1.cityId=c1.id AND c1.name = :name AND cw1.type = 'CURRENT' "
			+ "AND cw1.requestTime = (SELECT MAX(cw2.requestTime) FROM CityWeather cw2 WHERE cw2.type = 'CURRENT')")
	List<CityWeather> findCurrentWeatherByName(@Param("name") String name);

	@Query("SELECT cw1 FROM CityWeather cw1, City c1 WHERE cw1.cityId=c1.id AND c1.name = :name AND cw1.type = 'FORECAST' "
			+ "AND cw1.requestTime = (SELECT MAX(cw2.requestTime) FROM CityWeather cw2 WHERE cw2.type = 'FORECAST')")
	List<CityWeather> findForecastWeatherByName(@Param("name") String name);

}
