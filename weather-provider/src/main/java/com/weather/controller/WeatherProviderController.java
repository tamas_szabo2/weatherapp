package com.weather.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.data.model.entity.City;
import com.weather.data.model.entity.CityWeather;
import com.weather.service.WeatherProvidingService;

@RestController
public class WeatherProviderController {

    private static final String EMPTY_RESPONSE = "{}";

    private WeatherProvidingService weatherProvidingService;

    @Autowired
    public WeatherProviderController(WeatherProvidingService weatherProvidingService) {
        this.weatherProvidingService = weatherProvidingService;
    }

    @GetMapping(value = "/currentweather")
    public String getCurrentWeather(@RequestParam(value = "city") String city) {
        String response = EMPTY_RESPONSE;

        List<CityWeather> cityWeatherList = weatherProvidingService.getWeatherRepository()
                .findCurrentWeatherByName(city);

        try {
            response = Optional.ofNullable(new ObjectMapper().writeValueAsString(cityWeatherList)).orElse(EMPTY_RESPONSE);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return response;
    }

    @GetMapping(value = "/forecastweather")
    public String getForecastWeather(@RequestParam(value = "city") String city) {
        String response = EMPTY_RESPONSE;
        List<CityWeather> cityWeatherList = weatherProvidingService.getWeatherRepository()
                .findForecastWeatherByName(city);
        try {
            response = Optional.ofNullable(new ObjectMapper().writeValueAsString(cityWeatherList)).orElse(EMPTY_RESPONSE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @GetMapping(value = "/getcities")
    public String getCities() {
        String response = EMPTY_RESPONSE;
        List<City> cityWeatherList = weatherProvidingService.getCityRepository().findAll();
        try {
            response = Optional.ofNullable(new ObjectMapper().writeValueAsString(cityWeatherList)).orElse(EMPTY_RESPONSE);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return response;
    }

}
