package com.weather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.weather.repo.CityRepository;
import com.weather.repo.WeatherRepository;

/**
 * @author Ehtiram_Abdullayev on 9/24/2018
 * @project weatherapp
 */
@Service
public class WeatherProvidingService {

	@Autowired
	private WeatherRepository weatherRepository;

	@Autowired
	private CityRepository cityRepository;

	public WeatherRepository getWeatherRepository() {
		return weatherRepository;
	}

	public CityRepository getCityRepository() {
		return cityRepository;
	}

}
