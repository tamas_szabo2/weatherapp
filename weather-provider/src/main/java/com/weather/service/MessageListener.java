package com.weather.service;

import java.io.IOException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.data.model.entity.CityWeather;

@Service
public class MessageListener {

	@Autowired
	private WeatherProvidingService weatherProvidingService;

	@KafkaListener(topics = "weather_topic")
	public void receiveMessage(ConsumerRecord<?, ?> consumerRecord) throws IOException {
		String receivedMessage = consumerRecord.value().toString();
		ObjectMapper objectMapper = new ObjectMapper();
		CityWeather cityWeather = objectMapper.readValue(receivedMessage, CityWeather.class);

		weatherProvidingService.getWeatherRepository().saveAndFlush(cityWeather);
	}
}
