#!/bin/bash

PROJECT_ROOT=$PWD

echo "PROJECT ROOT: $PROJECT_ROOT"

echo ===============================================
echo INSTALLING GIT AND OTHER BASE ITEMS ===========
echo ===============================================
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get -y install git curl software-properties-common openjdk-8-jdk maven

echo ===============================================
echo INSTALLING DOCKER =============================
echo ===============================================
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update -y
sudo apt-get install -y docker-ce
sudo systemctl --no-pager status docker
sudo usermod -aG docker $USER
newgrp docker << ENDSCRIPT

echo ===============================================
echo INSTALLING DOCKER COMPOSE =====================
echo ===============================================

sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo docker-compose --version

echo ===============================================
echo CLONING PROJECT REPOSITORY ====================
echo ===============================================

cd $PROJECT_ROOT
git clone https://bitbucket.org/tamas_szabo2/weatherapp.git

echo ===============================================
echo CLONING KAFKA DOCKER ==========================
echo ===============================================

cd $PROJECT_ROOT/weatherapp/build/dockerfiles/
git clone https://github.com/wurstmeister/kafka-docker

echo ===============================================
echo COMPILING PROJECT CODE=========================
echo ===============================================
cd $PROJECT_ROOT/weatherapp/build/maven
mvn clean package

echo ===============================================
echo BUILDING DOCKER IMAGES ========================
echo ===============================================
cd $PROJECT_ROOT/weatherapp/build/compose
docker-compose build

echo ===============================================
echo SETTING UP LOGGING ============================
echo ===============================================
sudo mkdir -p /var/log/weather-demo
sudo chown ${USER:=$(/usr/bin/id -run)}:$USER /var/log/weather-demo
echo Application log files will be found under /var/log/weather-demo on the Docker host!

echo ===============================================
echo SETTING UP H2 DB ============================
echo ===============================================
sudo mkdir -p /opt/weatherapp
sudo chown ${USER:=$(/usr/bin/id -run)}:$USER /opt/weatherapp
echo H2 db file will be found under /opt/weatherapp

cd $PROJECT_ROOT

ENDSCRIPT

