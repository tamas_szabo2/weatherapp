package com.weather.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@RestController
public class WeatherAppController {

    private static final String PATH = "/producer/%s";

    private static final String PATH_WITH_CITY_PARAM = PATH + "?city=%s";

    private static final String ZUUL_SERVICE = "ZUUL-SERVICE";

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping(value = "/currentweather")
    public String getCurrentWeather(@RequestParam("city") String city) {
        return getWeather(String.format(PATH_WITH_CITY_PARAM, "currentweather", city));
    }

    @GetMapping(value = "/forecastweather")
    public String getForecastWeather(@RequestParam("city") String city) {
        return getWeather(String.format(PATH_WITH_CITY_PARAM, "forecastweather", city));
    }

    @GetMapping(value = "/getcities")
    public String getCities() {
        return getWeather(String.format(PATH, "getcities"));
    }

    private String getWeather(String path) {
        List<ServiceInstance> instances = discoveryClient.getInstances(ZUUL_SERVICE);
        ServiceInstance serviceInstance = instances.get(0);
        String baseUrl = serviceInstance.getUri().toString() + path;
        ResponseEntity<String> response = new RestTemplate().exchange(baseUrl, HttpMethod.GET, getHeaders(), String.class);
        return Optional.ofNullable(response).orElseThrow(() -> new RestClientException("Can't get the record ")).getBody();
    }

    private static HttpEntity<?> getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return new HttpEntity<>(headers);
    }

}
