package com.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.client.RestClientException;

@SpringBootApplication
@EnableDiscoveryClient
public class WeatherApplication {

    public static void main(String[] args) throws RestClientException {
        SpringApplication.run(WeatherApplication.class, args);
    }

}
