package com.weather.data.model.entity;

import java.util.List;

/**
 * @author Ehtiram_Abdullayev on 10/2/2018
 * @project weatherapp
 */
public class GeneralWeatherModel {

	private Integer count;

	private List<CityWeather> cityWeatherList;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<CityWeather> getCityWeatherList() {
		return cityWeatherList;
	}

	public void setCityWeatherList(List<CityWeather> cityWeatherList) {
		this.cityWeatherList = cityWeatherList;
	}

	@Override
	public String toString() {
		return "GeneralWeatherModel{" + "count=" + count + ", cityWeatherList=" + cityWeatherList + '}';
	}
}
