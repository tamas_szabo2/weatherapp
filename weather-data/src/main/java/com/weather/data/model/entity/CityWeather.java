package com.weather.data.model.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Ehtiram_Abdullayev on 10/2/2018
 * @project weatherapp
 */
@Entity
@Table
public class CityWeather {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long uid;

    private Long requestTime;

    @Enumerated(EnumType.STRING)
    private WeatherInfoType type;

    private Long dt;

    private String temperature;

    private String humidity;

    private Long cityId;

    private String weatherInfo;

    private String weatherInfoDesc;

    private String icon;

    private Double uvData;

    @JsonIgnore
    @Transient
    private String lon;

    @JsonIgnore
    @Transient
    private String lat;

    @PrePersist
    public void prePersist() {
        this.requestTime = System.currentTimeMillis() / 1000;
    }

    // setters and getters
    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }

    public WeatherInfoType getType() {
        return type;
    }

    public void setType(WeatherInfoType type) {
        this.type = type;
    }

    public Long getDt() {
        return dt;
    }

    public void setDt(Long dt) {
        this.dt = dt;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(String weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    public String getWeatherInfoDesc() {
        return weatherInfoDesc;
    }

    public void setWeatherInfoDesc(String weatherInfoDesc) {
        this.weatherInfoDesc = weatherInfoDesc;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Double getUvData() {
        return uvData;
    }

    public void setUvData(Double uvData) {
        this.uvData = uvData;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "CityWeather{" + "uid=" + uid + ", requestTime=" + requestTime + ", type=" + type + ", dt=" + dt
                + ", temperature='" + temperature + '\'' + ", humidity='" + humidity + '\'' + ", weatherInfo='"
                + weatherInfo + '\'' + ", weatherInfoDesc='" + weatherInfoDesc + '\'' + ", icon='" + icon + '\'' + '}';
    }
}
