package com.weather.data.model.dto;

/**
 * @author Ehtiram_Abdullayev on 10/2/2018
 * @project weatherapp
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "coord", "sys", "weather", "main", "visibility", "wind", "clouds", "dt", "id", "name" })
public class CityWeatherDTO {

	@JsonProperty("coord")
	private CoordDTO coord;

	@JsonProperty("sys")
	private SysDTO sys;

	@JsonProperty("weather")
	private List<WeatherDTO> weather = null;

	@JsonProperty("main")
	private MainDTO main;

	@JsonProperty("visibility")
	private Integer visibility;

	@JsonProperty("wind")
	private WindDTO wind;

	@JsonProperty("clouds")
	private CloudsDTO clouds;

	@JsonProperty("dt")
	private Integer dt;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("name")
	private String name;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("coord")
	public CoordDTO getCoord() {
		return coord;
	}

	@JsonProperty("coord")
	public void setCoord(CoordDTO coord) {
		this.coord = coord;
	}

	@JsonProperty("sys")
	public SysDTO getSys() {
		return sys;
	}

	@JsonProperty("sys")
	public void setSys(SysDTO sys) {
		this.sys = sys;
	}

	@JsonProperty("weather")
	public List<WeatherDTO> getWeather() {
		return weather;
	}

	@JsonProperty("weather")
	public void setWeather(List<WeatherDTO> weather) {
		this.weather = weather;
	}

	@JsonProperty("main")
	public MainDTO getMain() {
		return main;
	}

	@JsonProperty("main")
	public void setMain(MainDTO main) {
		this.main = main;
	}

	@JsonProperty("visibility")
	public Integer getVisibility() {
		return visibility;
	}

	@JsonProperty("visibility")
	public void setVisibility(Integer visibility) {
		this.visibility = visibility;
	}

	@JsonProperty("wind")
	public WindDTO getWind() {
		return wind;
	}

	@JsonProperty("wind")
	public void setWind(WindDTO wind) {
		this.wind = wind;
	}

	@JsonProperty("clouds")
	public CloudsDTO getClouds() {
		return clouds;
	}

	@JsonProperty("clouds")
	public void setClouds(CloudsDTO clouds) {
		this.clouds = clouds;
	}

	@JsonProperty("dt")
	public Integer getDt() {
		return dt;
	}

	@JsonProperty("dt")
	public void setDt(Integer dt) {
		this.dt = dt;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
