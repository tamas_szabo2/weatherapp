package com.weather.data.model.entity;

/**
 * @author Ehtiram_Abdullayev on 9/28/2018
 * @project weatherapp
 */
public enum WeatherInfoType {
	CURRENT, FORECAST
}
