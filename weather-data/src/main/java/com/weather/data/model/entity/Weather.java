
package com.weather.data.model.entity;

/**
 * @author Ehtiram_Abdullayev on 10/2/2018
 * @project weatherapp
 */

public class Weather {

	private String main;

	private String description;

	private String icon;

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
