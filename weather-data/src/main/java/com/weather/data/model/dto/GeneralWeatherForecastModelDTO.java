package com.weather.data.model.dto;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ehtiram_Abdullayev on 10/5/2018
 * @project weatherapp
 */
public class GeneralWeatherForecastModelDTO extends GeneralWeatherModelDTO {

	@JsonProperty("city")
	private CityDTO city;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("city")
	public CityDTO getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(CityDTO city) {
		this.city = city;
	}

	@JsonAnyGetter
    @Override
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	@Override
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
