package com.weather.data.model.dto;

/**
 * @author Ehtiram_Abdullayev on 10/2/2018
 * @project weatherapp
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "cnt", "list" })

public class GeneralWeatherModelDTO {

	@JsonProperty("cnt")
	private Integer cnt;

	@JsonProperty("list")
	private List<CityWeatherDTO> cityWeatherListDTO = null;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();

	@JsonProperty("cnt")
	public Integer getCnt() {
		return cnt;
	}

	@JsonProperty("cnt")
	public void setCnt(Integer cnt) {
		this.cnt = cnt;
	}

	@JsonProperty("list")
	public List<CityWeatherDTO> getList() {
		return cityWeatherListDTO;
	}

	@JsonProperty("list")
	public void setList(List<CityWeatherDTO> cityWeatherList) {
		this.cityWeatherListDTO = cityWeatherList;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
