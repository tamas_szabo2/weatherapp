package com.weather.data.model.entity.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.weather.data.model.dto.CityWeatherDTO;
import com.weather.data.model.dto.GeneralWeatherForecastModelDTO;
import com.weather.data.model.dto.GeneralWeatherModelDTO;
import com.weather.data.model.dto.WeatherDTO;
import com.weather.data.model.entity.CityWeather;
import com.weather.data.model.entity.GeneralWeatherModel;

/**
 * @author Ehtiram_Abdullayev on 10/2/2018
 * @project weatherapp
 */
@Mapper(componentModel = "spring")
public interface GeneralWeatherMapper {
    GeneralWeatherMapper MAPPER = Mappers.getMapper(GeneralWeatherMapper.class);

    @Mappings({ @Mapping(target = "count", source = "cnt"),
            @Mapping(target = "cityWeatherList", expression = "java(mapCityWeatherDTOToCityWeather(generalWeatherModelDTO.getList()))") })
    GeneralWeatherModel toGeneralWeatherModel(GeneralWeatherModelDTO generalWeatherModelDTO);

    @IterableMapping(elementTargetType = CityWeather.class)
    List<CityWeather> mapCityWeatherDTOToCityWeather(List<CityWeatherDTO> cityWeatherDTOS);

    @Mappings({ @Mapping(target = "temperature", source = "main.temp"),
            @Mapping(target = "humidity", source = "main.humidity"),
            @Mapping(target = "weatherInfo", expression = "java(toWeatherInfo(cityWeatherDTO.getWeather()))"),
            @Mapping(target = "weatherInfoDesc", expression = "java(toWeatherDesc(cityWeatherDTO.getWeather()))"),
            @Mapping(target = "icon", expression = "java(toWeatherIcon(cityWeatherDTO.getWeather()))"),
            @Mapping(target = "cityId", source = "id"), @Mapping(target = "lat", source = "coord.lat"),
            @Mapping(target = "lon", source = "coord.lon") })
    CityWeather toCityWeather(CityWeatherDTO cityWeatherDTO);

    @Mappings({
            @Mapping(target = "cityWeatherList", expression = "java(fromDtoCityWeatherListToGeneralOne(generalWeatherForecastModelDTO))") })
    GeneralWeatherModel toGeneralWeatherModel(GeneralWeatherForecastModelDTO generalWeatherForecastModelDTO);

    default String toWeatherInfo(List<WeatherDTO> weatherDTOS) {
        return weatherDTOS.stream().findFirst().orElse(new WeatherDTO()).getMain();
    }

    default String toWeatherDesc(List<WeatherDTO> weatherDTOS) {
        return weatherDTOS.stream().findFirst().orElse(new WeatherDTO()).getDescription();
    }

    default String toWeatherIcon(List<WeatherDTO> weatherDTOS) {
        return weatherDTOS.stream().findFirst().orElse(new WeatherDTO()).getIcon();
    }

    default List<CityWeather> fromDtoCityWeatherListToGeneralOne(
            GeneralWeatherForecastModelDTO generalWeatherModelDTO) {
        List<CityWeather> cityWeatherList = mapCityWeatherDTOToCityWeather(generalWeatherModelDTO.getList());
        cityWeatherList.forEach(cityWeather -> cityWeather.setCityId(generalWeatherModelDTO.getCity().getId()));
        return cityWeatherList;
    }
}
