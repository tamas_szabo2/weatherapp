package com.weather.service;

import java.util.List;
import com.weather.data.model.entity.GeneralWeatherModel;

/**
 * @author Ehtiram_Abdullayev on 9/21/2018
 * @project weatherapp
 */
public interface WeatherService {

	GeneralWeatherModel getCurrentWeatherModel();

	List<GeneralWeatherModel> getFiveDaysForecastWeather();

	Double getCurrentUVData(String lat, String lon);
}
