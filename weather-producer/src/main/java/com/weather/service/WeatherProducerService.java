package com.weather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.data.model.entity.GeneralWeatherModel;

@Service
public class WeatherProducerService {

    private static final String KAFKA_TOPIC = "weather_topic";

    private WeatherService weatherService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    public WeatherProducerService(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Scheduled(cron = "${currentWeatherCronExpression}")
    public void currentWeather() {
        GeneralWeatherModel model = weatherService.getCurrentWeatherModel();

        if (model != null) {
            model.getCityWeatherList().forEach(city -> {
                try {
                    kafkaTemplate.send(KAFKA_TOPIC, objectMapper.writeValueAsString(city));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @Scheduled(cron = "${forecastWeatherCronExpression}")
    public void forecastWeather() {
        weatherService.getFiveDaysForecastWeather()
                .forEach(weatherModelObj -> weatherModelObj.getCityWeatherList().forEach(cityWeatherList -> {
                    try {
                        kafkaTemplate.send(KAFKA_TOPIC, objectMapper.writeValueAsString(cityWeatherList));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }));
    }

}
