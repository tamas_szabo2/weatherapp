package com.weather.service.imp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.data.model.dto.GeneralWeatherForecastModelDTO;
import com.weather.data.model.dto.GeneralWeatherModelDTO;
import com.weather.data.model.dto.UVDataDTO;
import com.weather.data.model.entity.City;
import com.weather.data.model.entity.GeneralWeatherModel;
import com.weather.data.model.entity.WeatherInfoType;
import com.weather.data.model.entity.mapper.GeneralWeatherMapper;
import com.weather.service.WeatherService;

/**
 * @author Ehtiram_Abdullayev on 9/21/2018
 * @project weatherapp
 */
@Service
public class WeatherServiceImpl implements WeatherService {

    private static final String COMMA = ",";

    private static final String EQUALS = "=";

    @Value("${currentWeatherUri}")
    private String currentWeatherUri;

    @Value("${forecastWeatherUri}")
    private String forecastWeatherUri;

    @Value("${apiKey}")
    private String apiKey;

    @Value("${currentWeatherUVData}")
    private String currentWeatherUVData;

    private final RestTemplate restTemplate;

    @Autowired
    private GeneralWeatherMapper generalWeatherMapper;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    public WeatherServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public GeneralWeatherModel getCurrentWeatherModel() {
        String cityIds = getCitiesFromDb().stream().map(id -> String.valueOf(id.getId()))
                .collect(Collectors.joining(COMMA));
        String url = currentWeatherUri.replace("{cityIDs}", cityIds);
        GeneralWeatherModel model = null;

        if (!cityIds.isEmpty()) {
            try {
                ResponseEntity<GeneralWeatherModelDTO> responseEntity = restTemplate.getForEntity(url + EQUALS + apiKey,
                        GeneralWeatherModelDTO.class);

                model = generalWeatherMapper.toGeneralWeatherModel(responseEntity.getBody());
            } catch (HttpClientErrorException e) {
                System.out.println(e + "Request url: " + url);
            }

            if (model != null && model.getCityWeatherList() != null) {
                model.getCityWeatherList().forEach(cityWeather -> {
                    cityWeather.setType(WeatherInfoType.CURRENT);
                    cityWeather.setUvData(getCurrentUVData(cityWeather.getLat(), cityWeather.getLon()));
                });
            }
        }

        return model;
    }

    @Override
    public List<GeneralWeatherModel> getFiveDaysForecastWeather() {
        List<GeneralWeatherModel> weatherByCities = new ArrayList<>();

        getCitiesFromDb().forEach(city -> {
            String url = forecastWeatherUri.replace("{cityID}", String.valueOf(city.getId()));
            ResponseEntity<GeneralWeatherForecastModelDTO> responseEntity;

            try {
                responseEntity = restTemplate.getForEntity(url + EQUALS + apiKey, GeneralWeatherForecastModelDTO.class);
                GeneralWeatherForecastModelDTO responseWeatherModel = responseEntity.getBody();
                GeneralWeatherModel convertedModel = generalWeatherMapper.toGeneralWeatherModel(responseWeatherModel);

                convertedModel.getCityWeatherList()
                        .forEach(cityWeather -> cityWeather.setType(WeatherInfoType.FORECAST));
                weatherByCities.add(convertedModel);
            } catch (HttpClientErrorException e) {
                System.out.println(e + "Request url: " + url);
            }
        });

        return weatherByCities;
    }

    @Override
    public Double getCurrentUVData(String lat, String lon) {
        String url = currentWeatherUVData.replace("{lat}", lat).replace("{lon}", lon);
        ResponseEntity<UVDataDTO> responseEntity = restTemplate.getForEntity(url + EQUALS + apiKey, UVDataDTO.class);
        UVDataDTO uvData = responseEntity.getBody();

        return uvData.getValue();

    }

    private List<City> getCitiesFromDb() {
        List<City> cities = new ArrayList<>();
        List<ServiceInstance> instances = discoveryClient.getInstances("weather-provider");
        ServiceInstance serviceInstance = instances.get(0);
        ResponseEntity<String> response = restTemplate.getForEntity(serviceInstance.getUri().toString() + "/getcities",
                String.class);

        try {
            cities = objectMapper.readValue(response.getBody(),
                    objectMapper.getTypeFactory().constructCollectionType(List.class, City.class));
        } catch (IOException e) {
            System.out.println(e);
        }

        return cities;
    }

    public GeneralWeatherMapper getGeneralWeatherMapper() {
        return generalWeatherMapper;
    }

    public void setGeneralWeatherMapper(GeneralWeatherMapper generalWeatherMapper) {
        this.generalWeatherMapper = generalWeatherMapper;
    }

}