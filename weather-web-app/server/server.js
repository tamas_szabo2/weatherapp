const express = require('express');
const httpProxyMiddleware = require('http-proxy-middleware');

var app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json());
// app.use(cors());

app.use(express.static('public/www'));

// app.use('**', /*'/weather**', */(req, res, next) => {
//   const proxyConfig = {
//     context: '**',
//     target: 'http://10.96.0.116:8080', // http://10.96.0.116:8080/weather?city=Szeged
//     secure: false,
//     changeOrigin: true,
//   }
//   const context = proxyConfig.context || proxyConfig.path;
//   if (proxyConfig.target) {
//     httpProxyMiddleware(context, proxyConfig);
//   }
// });

// pathRewrite: {
//   '^/api/old-path' : '/api/new-path',     // rewrite path
//   '^/api/remove/path' : '/path'           // remove base path
// },

app.use(httpProxyMiddleware('**', {
  context: '**',
  target: 'http://weather-app:8082', // http://10.96.0.116:8080/weather?city=Szeged
  secure: false,
  changeOrigin: true,
}));


app.listen(3000, '0.0.0.0');
