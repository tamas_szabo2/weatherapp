import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { PlotlyHelperService } from 'app/services/plotly-helper.service';

@Component({
  selector: 'wa-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public data: any;
  private chartElement: any;

  constructor(private plotlyHelper: PlotlyHelperService) { }

  ngOnInit() {
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    this.chartElement = document.getElementById('scattergeoMapWithMarkers');
    if (simpleChanges.data.currentValue) {
      const mapData = {
        text: [this.data.city.name],
        lon: [this.data.city.lon],
        lat: [this.data.city.lat],
      };
      this.plotlyHelper.scattergeoMapWithMarkers(mapData, this.chartElement);
    }
  }

  ngOnDestroy() {
    this.plotlyHelper.removeChart(this.chartElement);
  }

}
