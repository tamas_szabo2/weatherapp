import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wa-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.scss']
})
export class CurrentWeatherComponent implements OnInit {
  @Input() public weatherData: any;
  @Input() public selectedCityData: any;

  constructor() { }

  ngOnInit() {
  }

}
