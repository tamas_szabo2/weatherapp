import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'wa-select-menu',
  templateUrl: './select-menu.component.html',
  styleUrls: ['./select-menu.component.scss']
})
export class SelectMenuComponent implements OnInit {
  @Input() public items = [];
  @Output() public selectedItem = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  public geSelectedItem(selectedItem): void {
    this.selectedItem.emit(selectedItem.value);
  }

}
