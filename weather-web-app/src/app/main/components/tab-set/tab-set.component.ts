import { Component, OnInit, SimpleChanges, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'wa-tab-set',
  templateUrl: './tab-set.component.html',
  styleUrls: ['./tab-set.component.scss']
})
export class TabSetComponent implements OnInit, OnChanges {
  @Input() public selectedCityData: any;
  @Input() public selectedCityForecast: any;
  public weatherData: any;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.selectedCityData.currentValue) {
      this.weatherData = {
        temperature: this.selectedCityData.temperature,
        humidity: this.selectedCityData.humidity,
        weatherInfo: this.selectedCityData.weatherInfo,
        weatherInfoDesc: this.selectedCityData.weatherInfoDesc,
        icon: this.selectedCityData.icon,
        iconPath: `../../../content/images/${this.selectedCityData.icon}.png`,
      };
    }
  }

}
