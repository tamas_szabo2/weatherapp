import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { PlotlyHelperService } from 'app/services/plotly-helper.service';

@Component({
  selector: 'wa-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public data: any = [];
  private chartElement: any;

  constructor(private plotlyHelper: PlotlyHelperService) { }

  ngOnInit() {
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    this.chartElement = document.getElementById('twoYAxis');
    if (simpleChanges.data.currentValue) {
      this.plotlyHelper.twoYAxis(this.data, this.chartElement);
    }
  }

  ngOnDestroy() {
    this.plotlyHelper.removeChart(this.chartElement);
  }

}
