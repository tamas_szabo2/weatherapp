import { Component, OnInit } from '@angular/core';
import { RestService } from 'app/services/rest.service';

@Component({
  selector: 'wa-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  public cityNames = [];
  public selectedCityData: any;
  public selectedCityForecast: any;

    constructor(private restService: RestService) {
      this.restService.getCities().subscribe(cities => {
        this.restService.cityData = cities;
        this.cityNames = cities.map(city => ({
            value: city.name
          }));
      });
    }

    ngOnInit() {
    }

    public getSelectedItem(cityName: string): void {
      this.restService.getWeatherData(cityName).subscribe(result => {
        this.selectedCityForecast = result[1];
        this.selectedCityData = result[0][0];
      });
    }
}
