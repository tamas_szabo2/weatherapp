import '../vendor';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MainComponent } from 'app/main/main.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import {
  NgbTabsetModule,
} from '@ng-bootstrap/ng-bootstrap';
import { TabSetComponent } from './main/components/tab-set/tab-set.component';
import { SelectMenuComponent } from './main/components/select-menu/select-menu.component';
import { ForecastComponent } from './main/components/forecast/forecast.component';
import { MapComponent } from './main/components/map/map.component';
import { CurrentWeatherComponent } from './main/components/current-weather/current-weather.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTabsetModule,
    FontAwesomeModule,
  ],
  declarations: [
    MainComponent,
    TabSetComponent,
    SelectMenuComponent,
    ForecastComponent,
    MapComponent,
    CurrentWeatherComponent,
  ],
  providers: [
  ],
  bootstrap: [MainComponent],
})
export class AppModule {}
