import { Injectable } from '@angular/core';
import * as Plotly from 'plotly.js';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class PlotlyHelperService {

  constructor() { }

  public removeChart(elementOfChart: HTMLElement): void {
    Plotly.purge(elementOfChart);
  }

  public twoYAxis(inputData: any, chartPlace: HTMLElement) {
    const data = {
      temperature: [
        ...inputData.map(forecastItem => (forecastItem.temperature)),
      ],
      humidity: [
        ...inputData.map(forecastItem => forecastItem.humidity),
      ],
      time: [
        ...inputData.map(forecastItem => moment.unix(forecastItem.dt).format('YYYY-MM-DD HH:MM:SS')),
      ],
    };
    const trace1 = {
      x: [...data.time],
      y: [...data.temperature],
      name: 'Temperature',
      type: 'scatter'
    };

    const trace2 = {
      x: [...data.time],
      y: [...data.humidity],
      name: 'Humidity',
      yaxis: 'y2',
      type: 'scatter'
    };

    const chartData = [trace1, trace2];

    const layout = {
      title: 'Forecast',
      yaxis: {
        title: 'Temperature °C',
        range: [0, 40],
      },
      yaxis2: {
        title: 'Humidity %',
        titlefont: {
          color: 'rgb(148, 103, 189)'
        },
        tickfont: {
          color: 'rgb(148, 103, 189)'
        },
        overlaying: 'y',
        side: 'right',
        range: [20, 100],
      }
    };

    // @ts-ignore: problem with 'scatter' type
    Plotly.react(chartPlace, chartData, layout, { responsive: true });
  }

  public scattergeoMapWithMarkers(data: any, mapPlace: HTMLElement) {
    const chartData = [{
        type: 'scattergeo',
        mode: 'markers+text',
        text: [
            ...data.text,
        ],
        lon: [
          ...data.lon,
        ],
        lat: [
          ...data.lat,
        ],
        marker: {
            size: 7,
            color: [
                '#fdb462', '#fb8072',
            ],
            line: {
                width: 1
            },
            ...data.marker,
        },
        name: '',
        textposition: [
          'top right', 'top left', 'top center', 'bottom right', 'top right',
          'top left', 'bottom right', 'bottom left', 'top right', 'top right',
        ],
    }];

    const layout = {
        autosize: true,
        title: '',
        width: 400,
        height: 300,
        zoom: 40,
        margin: {
          r: 10,
          t: 10,
          b: 10,
          l: 10,
          pad: 0,
        },
        font: {
          family: 'Droid Serif, serif',
          size: 16
        },
        titlefont: {
          size: 16
        },
        geo: {
          scope: 'europe',
          resolution: 100,
          lonaxis: {
              'range': [data.lon[0] - 30, data.lon[0] + 30]
          },
          lataxis: {
              'range': [data.lat[0] - 10, data.lat[0] + 10]
          },
          showrivers: true,
          rivercolor: '#fff',
          showlakes: true,
          lakecolor: '#fff',
          showland: true,
          landcolor: '#d4d4d4',
          countrycolor: '#969696',
          countrywidth: 1.5,
          subunitcolor: '#969696'
        }
    };

    // @ts-ignore: 'scattergeo' type is missing from the interface for some reason
    Plotly.react(mapPlace, chartData, layout);
  }
}
