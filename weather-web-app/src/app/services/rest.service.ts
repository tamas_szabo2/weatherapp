import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

/*
    https://api.openweathermap.org/data/2.5/group?appid=a6de0bba41c25daaf7bb2b4c84eaef34&id=715429,2825297,625144&units=metric
    https://api.openweathermap.org/data/2.5/forecast?id=625144&appid=a6de0bba41c25daaf7bb2b4c84eaef34
*/

@Injectable({
  providedIn: 'root'
})
export class RestService {
  private appId = 'a6de0bba41c25daaf7bb2b4c84eaef34';
  public cityData = [];

  constructor(private http: HttpClient) { }

  public getCities(): Observable<any> {
    return this.http.get('/getcities');
  }

  public getWeatherData(cityName: string): Observable<any> {
    return forkJoin(
      this.http.get(`/currentweather?city=${cityName}`)
        .pipe(
          map((response: Array<any>) => {
            return response.map(current => {
              const cityData = this.cityData.find(city => city.id === current.cityId);
              return {
                ...current,
                city: cityData,
              };
            });
          }),
          catchError(() => of(''))
        ),
      this.http.get(`/forecastweather?city=${cityName}`)
        .pipe(
          catchError(() => of(''))
        ),
    );
  }

}
