#!/bin/bash

PROJECT_ROOT=$PWD

echo "Project Root: $PROJECT_ROOT"

echo REFRESHING PROJECT GIT REPOSITORY =============
cd $PROJECT_ROOT
git pull

echo ===============================================
echo COMPILING PROJECT CODE=========================
echo ===============================================
cd $PROJECT_ROOT/build/maven
mvn clean package

echo BUILDING DOCKER IMAGES ========================
cd $PROJECT_ROOT/build/compose
docker image prune -af
docker-compose build
docker-compose up --force-recreate

